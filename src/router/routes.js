
export default  ([
    {
        path      : '/login',
        name      : 'Login',
        component : require('../views/login.vue').default
    },
    {
        path      : '/',
        name      : 'dashboard',
        component : require('../views/Dashboard.vue').default,
        children : [
            {
                path      : '/Users',
                name      : 'Users',
                component : require('../views/Users.vue').default,
            },
        ]

    },
    
])
