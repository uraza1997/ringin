import firebase from "firebase";

const firebaseConfig = {
    apiKey: "AIzaSyARynwl_3Nfifsj7kRXOE_NsKVM-CFSkzg",
    authDomain: "ring-in-app.firebaseapp.com",
    databaseURL: "https://ring-in-app.firebaseio.com",
    projectId: "ring-in-app",
    storageBucket: "ring-in-app.appspot.com",
    messagingSenderId: "297165891660",
    appId: "1:297165891660:web:0761a16b99a6c61e5bace9",
    measurementId: "G-BZHGW81LDZ"
  };

export default firebase.initializeApp(firebaseConfig);
